# Relative Velocity's running resources!

This repo contains:

* My [running build templates](./Templates)
* My [running guide](https://guildwarslegacy.com/forum/thread/3231/)
* God of Fissures' old [sulfur texmod](./EC-DesolationMod.tpf)
* A [texmod map mod](./running.tpf) for running routes and notable
  skill timings in Guild Wars.

  The skill timings were made without HSR or HCT, but under the
  assumption of a D/A runner's speed and a natural use of shadow steps
  to get ahead. You may need to tweak skill timings on a slower build.

  The routes also assume you have the quest states neccesary to trigger
  certain spawns. For more info, see the guide.

## Usage

Download this repo with the download button at the top right

Drop the templates in your templates folder

Open your Mission Map (Default key U) move it near your minimap,
and **zoom in all the way**. This is more important than it
seems. There are a lot of very small details in the line.

Skill icons on the map are skill timings you will want to adhere to,
and greyscale skill icons mean that you might have to use them
depending on upcoming spawns.

For example, you may never see an Ice Golem along the route through
Drakkar Lake, but if you do you'll need to use SF.

On the other hand, Lornar's Pass timings are largely dictated by
spawns, so you'll have to figure out SF on your own for most of it.

On routes where you can run both ways, skill icons will have an
arrow next to them indicating which direction that skill use is for.

A DS icon with a number on it indicates you should pop alcohol worth
that many points and dwarven stability there for KD-prevention.

A grey DS icon means you might have enchantment stripping up ahead.
You might want to skip DS and run at normal speed, rather than get
stripped and lose more time.

## Samples

![Mission Map](https://gitlab.com/jnvsor/gw-running-mapmods/raw/master/sample.png)  
![World Map](https://gitlab.com/jnvsor/gw-running-mapmods/raw/master/sample2.png)
